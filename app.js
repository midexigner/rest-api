const express = require('express');
const app = express();
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const cors = require("cors")
require('dotenv/config')
const Port = process.env.PORT || 5000;

// middleware
app.use(cors())
app.use(bodyParser.json())
/* app.use('/posts',()=>{
    console.log("this is a middleware running")
}) */
//  import middlewares Routes
const authRoutes = require("./routes/auth")
const postsRoutes = require("./routes/posts")
app.use('/api/user',authRoutes);
app.use('/api/posts',postsRoutes);
// ROUTES
app.get('/',(req,res)=>{
res.send("We are on Home")
})


// connect to DB
mongoose
  .connect(
    process.env.DB_CONNECTION,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex:true
    }
  ).then(() => console.warn("db Connection"));

// db connection end

// listen
app.listen(Port, () =>
  console.log(`server address: https://localhost:${Port}`)
);
